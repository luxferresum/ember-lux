import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
// import StrictModeComponent from 'id-helper/'

module('Integration | Helper | id', function(hooks) {
  setupRenderingTest(hooks);

  // Replace this with your real tests.
  test('it assigns the same id in the same context', async function(assert) {
    await render(hbs`
      <Test />
    `);

    const label = document.querySelector('label');
    const input = document.querySelector('input');
    assert.equal(label.getAttribute('for'), input.getAttribute('id'), 'id for label and input is the same');
  });

  test('it does not assign the same id in a new context', async function(assert) {
    this.set('inputValue', '1234');

    await render(hbs`
      <div class="one">
        <Test />
      </div>
      <div class="two">
        <Test />
      </div>
    `);

    const one = document.querySelector('.one input');
    const two = document.querySelector('.two input');
    assert.notEqual(one.getAttribute('id'), two.getAttribute('id'), 'it must not be the same for a new context');
  });

  test('it works in strict mode', async function(assert) {
    await render(hbs`
      <StrictModeComponent />
    `);

    const label = document.querySelector('label');
    const input = document.querySelector('input');
    assert.equal(label.getAttribute('for'), input.getAttribute('id'), 'id for label and input is the same');
  });
});
